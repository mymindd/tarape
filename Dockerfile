FROM debian:sid
RUN echo 'deb http://ftp.sg.debian.org/debian/ sid main contrib non-free' > /etc/apt/sources.list
RUN apt update && apt upgrade -y
RUN apt install -y g++ gcc build-essential python3 python3-dev python3-pip python3-venv ca-certificates

COPY . /app
WORKDIR /app
RUN pip3 install flask
RUN python3 setup.py develop
ENV BUSSAYA_SETTINGS=/app/settings.cfg
ENV FLASK_ENV=prodoction
ENV AUTHLIB_INSECURE_TRANSPORT=true

CMD /app/scripts/run.sh
