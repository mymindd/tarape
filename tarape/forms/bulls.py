from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired
from wtforms import fields
from wtforms.fields import html5
from wtforms import validators
from wtforms import Form


class AddBullForm(FlaskForm):
    name = fields.TextField('Name',
                            validators=[validators.InputRequired()])
    detail = fields.TextAreaField('Detail', default='-')
    picture = fields.FileField('Bull Picture')
    price = fields.IntegerField('Price(THB)',
                            validators=[validators.InputRequired()])


class Test:
    pass
