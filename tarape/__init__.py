__version__ = '0.1'

from flask import Flask
# from flask_user import UserManager
# from flask_login import LoginManager

import tarape
from . import acl
from . import views
from . import models
from . import oauth2

# def init_login_manager(app):
    # user_manager = UserManager(app, models.db, models.User)

def create_app():
    app = Flask(__name__)
    app.config.from_object('tarape.default_settings')
    app.config.from_envvar('TARAPE_SETTINGS', silent=True)

    # init_login_manager(app)
    models.init_db(app)
    oauth2.init_oauth(app)
    acl.init_acl(app)
    views.register_blueprint(app)

    return app

