from flask_mongoengine import MongoEngine
from .users import User
from .bulls import Bull, BullTransaction, BullImage
# from .courses import (Course,
#                       CourseMember,
#                       CourseInstructor,
#                       CourseChapter,
#                       ContentFile)
from .oauth2 import OAuth2Token
from .hash import HashBull

db = MongoEngine()

__all__ = [User, OAuth2Token, Bull, BullTransaction, HashBull, BullImage]


def init_db(app):
    db.init_app(app)

def init_mongoengine(settings):
    import mongoengine as me
    dbname = settings.get('MONGODB_DB')
    host = settings.get('MONGODB_HOST', 'localhost')
    port = int(settings.get('MONGODB_PORT', '27017'))
    username = settings.get('MONGODB_USERNAME', '')
    password = settings.get('MONGODB_PASSWORD', '')

    me.connect(db=dbname,
               host=host,
               port=port,
               username=username,
               password=password)
