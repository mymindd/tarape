import mongoengine as me
import datetime
# from flask_login import UserMixin, LoginManager
from flask_login import UserMixin


class User(me.Document, UserMixin):
    meta = {'collection': 'users'}

    email_address = me.StringField(required=True, unique=True)
    password = me.StringField()

    firstname = me.StringField(required=True)
    lastname = me.StringField(required=True)
    points=me.IntField(required=True, default=5000)

    roles = me.ListField(me.StringField(), default=['user'])

    is_active = me.BooleanField(default=True)

    created_date = me.DateTimeField(required=True,
                                    default=datetime.datetime.now)
    update_date = me.DateTimeField(required=True,
                                   default=datetime.datetime.now)

