import mongoengine as me
import datetime


class HashBull(me.Document):
    meta = {'collection': 'hashbulls'}
    bull = me.ReferenceField('Bull', dbref=True)
    price = me.IntField(required=True)
    owner = me.ReferenceField('User', dbref=True)
    detail = me.StringField()
    buyer = me.ReferenceField('User', dbref=True)
    time_stamp = me.DateTimeField(required=True,
                                   default=datetime.datetime.now)
    hash_before = me.StringField(default='')
    hash_next = me.StringField(default='')
