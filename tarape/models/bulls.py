import mongoengine as me
import datetime

class BullTransaction(me.EmbeddedDocument):
    price = me.IntField(required=True, default=0)
    started_date = me.DateTimeField(required=True,
                                          default=datetime.datetime.now)
    is_active = me.BooleanField(default=False)

class BullImage(me.EmbeddedDocument):
    image = me.ImageField()
    uploaded_date = me.DateTimeField(default=datetime.datetime.now)


class Bull(me.Document):
    meta = {'collection': 'bulls'}

    name = me.StringField(required=True)
    owner = me.ReferenceField('User', dbref=True)
    picture = me.EmbeddedDocumentField("BullImage", default=BullImage())
    detail = me.StringField()
    bull_transaction = me.EmbeddedDocumentField("BullTransaction",
                                           default=BullTransaction())

    update_date = me.DateTimeField(required=True,
                                   default=datetime.datetime.now)
    created_date = me.DateTimeField(required=True,
                                    default=datetime.datetime.now)
