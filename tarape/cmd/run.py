import tarape
def main():
    app = tarape.create_app()

    app.run(
        debug=True,
        host='0.0.0.0',
        port=8080
    )

