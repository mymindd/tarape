from flask import (Blueprint, 
                   request, 
                   render_template, 
                   redirect, 
                   url_for,
                   current_app,
                   Flask)
from tarape import models
from tarape import oauth2
# from tarape.forms import LoginForm, RegisterForm
from flask_login import (UserMixin, 
                        login_required, 
                        current_user)

# from werkzeug.security import generate_password_hash, check_password_hash
# from flask import Flask, redirect, url_for, session, request, jsonify

import datetime


module = Blueprint('main', __name__, url_prefix='/')


@module.route('/')
def index():
    if current_user.is_authenticated:
        return redirect(url_for('dashboard.index'))
    return redirect(url_for('accounts.login'))
    
