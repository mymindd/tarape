from flask import (Blueprint, 
                   request,
                   Response,
                   abort,
                   send_file,
                   render_template, 
                   redirect, 
                   url_for,
                   current_app,
                   Flask)
from tarape import models
from tarape import oauth2
from tarape.forms import AddBullForm
from flask_login import (UserMixin, 
                        login_required, 
                        current_user)

import hashlib 
import datetime


module = Blueprint('bulls', __name__, url_prefix='/bulls')

@module.route('/', methods=['GET', 'POST'])
@login_required
def index():
    bulls = models.Bull.objects()
    return render_template("/bulls/index.html", user=current_user,
                                                bulls=bulls)

@module.route('/addbull', methods=['GET', 'POST'])
@login_required
def add_bull():
    form = AddBullForm()
    if not form.validate_on_submit():
        return render_template('bulls/addbull.html',
                               form=form,
                               user=current_user)
    # hours = [1, 3, 6]
    # form.auction_closed.choices = [(h, str(h)) for h in hours]
    transaction = models.BullTransaction(price=form.price.data,
                               started_date=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                               is_active = True)

    bull = models.Bull()
    bull.name=form.name.data
    bull.owner=current_user._get_current_object()
    bull.detail=form.detail.data
    bull.created_date=datetime.datetime.now()
    bull.update_date=datetime.datetime.now()
    bull.bull_transaction=transaction
    if form.picture.data:
        image = models.BullImage()
        image.image.put(form.picture.data,
                        filename=form.picture.data.filename)
        bull.picture = image
    bull.save()
    return redirect(url_for('bulls.mybull'))


@module.route('/buy/confirm/<bull_id>', methods=['GET', 'POST'])
@login_required
def confirm_buy(bull_id):
    bulls = models.Bull.objects.get(id=bull_id)
    if bulls.bull_transaction.is_active == False:
        return redirect(url_for('bulls.index'))
    if current_user.points < bulls.bull_transaction.price:
        return redirect(url_for('bulls.index'))
    hash_bull = models.HashBull.objects(bull=bulls).order_by('-id').first()
    if hash_bull is None:
        print('Empty')
        hash_data=hashlib.md5(str(bulls.name+str(bulls.bull_transaction.price)+bulls.owner.firstname+bulls.detail+current_user.firstname+str(datetime.datetime.now())+'').encode())
        new_hash = models.HashBull(bull=bulls,
                                   price=bulls.bull_transaction.price,
                                   owner=bulls.owner.id,
                                   detail=bulls.detail,
                                   buyer=current_user.id,
                                   time_stamp=datetime.datetime.now(),
                                   hash_before='',
                                   hash_next= str(hash_data)
                                   )
        
        new_hash.save()
    else:
        hash_data=hashlib.md5(str(bulls.name+str(bulls.bull_transaction.price)+bulls.owner.firstname+bulls.detail+current_user.firstname+str(datetime.datetime.now)+hash_bull.hash_next).encode())
        new_hash = models.HashBull(bull=bulls,
                                   price=bulls.bull_transaction.price,
                                   owner=bulls.owner.id,
                                   detail=bulls.detail,
                                   buyer=current_user.id,
                                   time_stamp=datetime.datetime.now(),
                                   hash_before=hash_bull.hash_next,
                                   hash_next= str(hash_data)
                                   )
        new_hash.save()

    bulls.owner.points += (bulls.bull_transaction.price)
    bulls.owner.save()
    bulls.owner=current_user._get_current_object()
    bulls.bull_transaction.is_active = False
    bulls.owner.points -= (bulls.bull_transaction.price)
    bulls.save()
    bulls.owner.save()
    return redirect(url_for('dashboard.index'))

@module.route('/cancel/<bull_id>', methods=['GET', 'POST'])
@login_required
def cancel(bull_id):
    bull = models.Bull.objects.get(id=bull_id)
    # if bull.bull_transaction.is_active == False:
        # return redirect(url_for('bulls.index'))
    # if current_user.points < bull.bull_transaction.price:
        # return redirect(url_for('bulls.index'))
    bull.bull_transaction.is_active = False
    bull.save()
    return redirect(url_for('bulls.mybull'))

@module.route('/sell/<bull_id>', methods=['GET', 'POST'])
@login_required
def sell(bull_id):
    bull = models.Bull.objects.get(id=bull_id)
    # if bull.bull_transaction.is_active == False:
        # return redirect(url_for('bulls.index'))
    # if current_user.points < bull.bull_transaction.price:
        # return redirect(url_for('bulls.index'))
    bull.bull_transaction.is_active = True
    bull.save()
    return redirect(url_for('bulls.mybull'))
 
@module.route('/buy/<bull_id>', methods=['GET', 'POST'])
@login_required
def buy(bull_id):
    bull = models.Bull.objects.get(id=bull_id)
    if bull.bull_transaction.is_active == False:
        return redirect(url_for('bulls.index'))
    if current_user.points < bull.bull_transaction.price:
        return redirect(url_for('bulls.index'))
   
    return render_template('bulls/buy.html',user=current_user,
                                                bull=bull)
@module.route('/mybull', methods=['GET', 'POST'])
@login_required
def mybull():
    bulls = models.Bull.objects(owner=current_user._get_current_object())
    return render_template('bulls/mybull.html', user=current_user,
                                                bulls=bulls)

@module.route('<bull_id>/<filename>', methods=["GET"])
@login_required
def get_image(bull_id, filename):
    bull = models.Bull.objects.get(id=bull_id)
    img = None
    if bull.picture.image.filename == filename:
        img = bull.picture

    response = Response()
    if img:
        response = send_file(img.image, mimetype='image/jpg')
    else:
        response.status_code = 404

    return response

