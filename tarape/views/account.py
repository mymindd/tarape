from flask import (Blueprint, 
                   request, 
                   render_template, 
                   redirect, 
                   url_for,
                   current_app,
                   Flask)
from tarape import models
from tarape import oauth2
from flask_login import (UserMixin, 
                        login_required, 
                        current_user,
                        logout_user)

from werkzeug.security import generate_password_hash, check_password_hash

import datetime

module = Blueprint('accounts', __name__, url_prefix='/')

@module.route('/login')
def login():
    # print('>>>',current_user.is_authenticated)
    if current_user.is_authenticated:
        # print('hello')
        return redirect(url_for('dashboard.index'))
    return render_template('/account/login.html')
    
@module.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('accounts.login'))
