from flask import Flask
from . import main
from . import account
from . import dashboard
from . import bulls
def register_blueprint(app):
    app.register_blueprint(main.module)
    app.register_blueprint(account.module)
    app.register_blueprint(dashboard.module)
    app.register_blueprint(bulls.module)
