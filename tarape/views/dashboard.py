from flask import (Blueprint, 
                   request, 
                   render_template, 
                   redirect, 
                   url_for,
                   current_app,
                   Flask)
from tarape import models
from tarape import oauth2
# from tarape.forms import LoginForm, RegisterForm
from flask_login import (UserMixin, 
                        login_required, 
                        current_user)

from werkzeug.security import generate_password_hash, check_password_hash
# from flask import Flask, redirect, url_for, session, request, jsonify

import datetime


module = Blueprint('dashboard', __name__, url_prefix='/dashboard')


@module.route('/')
@login_required
def index():
    # return render_template('/dashboard/index.html', user=current_user)
    return redirect(url_for('bulls.mybull'))
    
